import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import Sidebar from '@/components/nav/NavComonents.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '首页',
    meta: {
      title: "首页",
      roles: ['user', 'admin']
    },
    redirect: '/dashboard',
    leaf: true, //是否为单节点，增加属性
    menuShow: true,
    component: HomeView,
    icoClass: 'el-icon-s-home',
    children: [
      //管理首页
      {
        path: '/dashboard',
        name: "管理首页",
        meta: {
          title: "管理首页",
          roles: ['user', 'admin']
        },
        leaf: true, //是否为单节点，增加属性
        menuShow: true,
        icoClass: 'el-icon-s-home',
        components: {
          default: () => import('@/views/main/dashboard.vue'),
          sidebar: Sidebar
        }
      },
      //管理设置页面
      {
        path: '/setting',
        name: "个人信息",
        meta: {
          title: "个人信息",
          roles: ['user', 'admin']
        },
        components: {
          default: () => import('@/views/main/SetView.vue'),
          sidebar: Sidebar
        }
      },
      //权限403
      {
        path: '/403',
        name: "403",
        meta: {
          title: "碳中和管理平台-403无权限",
          roles: ['user', 'admin']
        },
        components: {
          default: () => import('@/views/main/403.vue'),
          sidebar: Sidebar
        }
      },
    ]
  },
  //用户管理
  {
    path: '/user',
    name: '用户管理',
    meta: {
      title: "用户管理",
      roles: ['admin']
    },
    redirect: '/user/usermanager',
    leaf: true, //是否为单节点，增加属性
    menuShow: true,
    component: HomeView,
    icoClass: 'el-icon-user',
    children: [
      {
      path: '/user/usermanager',
      name: "后台用户管理",
      meta: {
        title: "用户管理",
        roles: ['admin']
      },
      leaf: true, //是否为单节点，增加属性
      menuShow: true,
      icoClass: 'el-icon-user',
      components: {
        default: () => import('@/views/user/userManager.vue'),
        sidebar: Sidebar
      }
    },
  ]
  },
  //文档管理
  {
    path: '/docmanager',
    name: "文档管理",
    components: {
      default: () => import('@/views/HomeView.vue'),
      sidebar: Sidebar
    },
    meta: {
      title: "文档管理",
      roles: ['user', 'admin']
    },
    leaf: false, //是否为单节点，增加属性
    menuShow: true,
    icoClass: 'el-icon-document-checked',
    children: [
      //新建文档
      {
        path: '/docmanager/newdoc',
        name: "新建文档",
        meta: {
          title: "新建文档",
          roles: ['user', 'admin']
        },
        menuShow: true,
        components: {
          default: () => import('@/views/doc/newdoc.vue'),
          sidebar: Sidebar
        }
      },
      //添加文档
      {
        path: '/docmanager/add',
        name: "添加文档",
        meta: {
          title: "添加文档",
          roles: ['user', 'admin']
        },
        menuShow: true,
        components: {
          default: () => import('@/views/doc/adddoc.vue'),
          sidebar: Sidebar
        }
      },
    ]
  },
  {
    path: '/login',
    name: 'LoginView',
    meta: {
      title: "登录"
    },
    enabled: false,
    component: () => import('@/views/LoginView.vue')
  },
  {
    path: '/reg',
    name: 'RegView',
    meta: {
      title: "注册"
    },
    enabled: false,
    component: () => import('@/views/RegView.vue')
  }
]

//将路由表信息存放到store中,注意变量名称不要写错了
localStorage.setItem('route', JSON.stringify(routes));

const router = new VueRouter({
  routes
})


//路由验证登录
router.beforeEach(function (to, from, next) {

  var user = localStorage.getItem("token");
  let user_role = '';
  //先给user_role一个初始值，然后检查vuex中是否有用户信息，再进行赋值，以进行路由访问权限判定
  if (localStorage.getItem('User')) {
    user_role = JSON.parse(localStorage.getItem('User')).user_role ? JSON.parse(localStorage.getItem('User')).user_role : 'user';
  }


  if (to.path == '/reg' || to.path == '/login') {
    next()
    return
  }

  if (user != null || to.path === '/login') {
    if (to.meta.roles && !to.meta.roles.includes(user_role) && to.path !== '/login' && to.path !== '/reg') {
      //无权限，重定向到首页或者其他无权限页面
      next('/403')
    } else {
      //有权限，正常导航
      next();
    }
  } else {
    if (user === null || user === '') {
      next('/login');
    }
  }
});

export default router
