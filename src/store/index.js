import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 存储token
    token: localStorage.getItem('token') ? localStorage.getItem('token') : '',
    Authorization: localStorage.getItem('User') ? localStorage.getItem('User') : ''
  },
  getters: {
  },
  mutations: {
    //注册处理函数，在login组件的login方法上使用this.$store.commit('login',data);
    login(state, user) {
      state.Authorization = user;
      localStorage.setItem('User', user)
    },
    loginToken(state,token){
      state.token = token;
      localStorage.setItem('token', token)
    }
  },
  actions: {
  },
  modules: {
  }
})
