# 基于Vue 2.x 的后台管理框架模板

## 项目介绍
本项目为基于Vue 2.x版本的后台管理框架模板，集成了elementui、mavon-editor等第三方框架，修复了常规bug，大家可以以此为模板构建自己的后台管理系统。

![输入图片说明](img1.png)

## 项目初始化
```
npm install
```

### 项目编译运行
```
npm run serve
```

### 编译打包生产模式
```
npm run build
```

